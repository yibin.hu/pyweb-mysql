USE demodb;

DROP TABLE IF EXISTS demo_table;

CREATE TABLE IF NOT EXISTS demo_table
(
    id            INT AUTO_INCREMENT NOT NULL PRIMARY KEY, 
    customerName  VARCHAR( 255 ) NOT NULL, 
    effectiveDate DATE, 
    description   TEXT, 
    status        TINYINT NOT NULL
    );
