mpod=$(oc get pods --selector app=mysql --output name | awk -F/ '{print $NF}')

echo "Copying setup files into pod..."
oc cp ./create_demotable.sql $mpod:/tmp/create_demotable.sql
oc cp ./fill_demotable.sql ${mpod}:/tmp/fill_demotable.sql

echo "Creating table(s)..."
oc exec $mpod -- bash -c "mysql --user=root demodb < /tmp/create_demotable.sql"

echo "Importing data..."
oc exec $mpod -- bash -c "mysql --user=root < /tmp/create_demotable.sql"

echo "Here is your table:"
oc exec $mpod -- bash -c "mysql --user=root demodb -e 'use demodb; SELECT * FROM demo_table;'"

echo "Flushing privileges..."
oc exec $mpod -- bash -c "mysql --user=root -e 'FLUSH PRIVILEGES;'"
