import os, mysql.connector
from flask import Flask
application = Flask(__name__)

# read database credentails from environment
HOST = 'mysql'
TABLE='demo_table'
USER = os.environ['MYSQL_USER']
PASSWORD = os.environ['MYSQL_PASSWORD']
DB = os.environ['MYSQL_DATABASE']

conn = mysql.connector.connect(host=HOST, database=DB, user=USER, password=PASSWORD)

@application.route("/")
def greeting():
    return "Welcome to the demo app!"

@application.route("/write")
def write_to_db():
    cursor = conn.cursor(buffered=True)
    cursor.execute("SELECT * FROM {}".format(TABLE))
    res = cursor.fetchall()
    conn.commit()
    return str(res)

@application.route("/read")
def read_from_db():
    cursor = conn.cursor(buffered=True)
    cursor.execute("SELECT * FROM {}".format(TABLE))
    res = cursor.fetchall()
    conn.commit()
    return str(res)

if __name__ == "__main__":
    application.run()
    conn.close()
