FROM alpine:latest

# update and install python pip
RUN apk add --update python3 py-pip curl

# upgrade pip3
RUN pip3 install --upgrade pip

# install python libs
RUN pip3 install mysql-connector-python
RUN pip3 install tornado
#RUN pip3 install commands

# set environment variables
ENV MYSQLUSER='user'
ENV MYSQLPASSWORD='password'
ENV MYSQLDB='demodb'

# bundle app source
COPY demoapp.py /src/demoapp.py
EXPOSE 8888

# run app
ENTRYPOINT ["python3", "/src/demoapp.py", "-p 8888"]
